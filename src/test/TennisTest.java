package test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

import tennis.Player;

public class TennisTest {

	private Player playerA;
	
	private Player playerB;
	
	private Tennis tennisTest;
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	
	@Before
	public void initialize() {
		
		this.playerA = new Player("A");
		this.playerB = new Player("B");
		this.tennisTest = new Tennis(this.playerA, this.playerB);			
	}

	@Test
	public void testDisplayScorePlayerASuccess() {
		
		System.setOut(new PrintStream(outContent));
		this.tennisTest.update(this.playerA);
		
		assertEquals("15-0", outContent.toString());
	}
	
	@Test
	public void testDisplayScorePlayerBSuccess() {
		
		System.setOut(new PrintStream(outContent));
		this.tennisTest.update(this.playerB);
		
		assertEquals("0-15", outContent.toString());
	}
	
	@Test
	public void testDisplayScorePlayerAadvantageSuccess() {
		
		// both players have 40 points 
		for (int ii = 0; ii < 3; ii++) {
			this.tennisTest.update(this.playerA);
			this.tennisTest.update(this.playerB);
		}
		
		// playerA wins one more time
		System.setOut(new PrintStream(outContent));
		this.tennisTest.update(this.playerA);		
	
		assertEquals("40A-40", outContent.toString());
	}
	
	@Test
	public void testDisplayScorePlayerBadvantageSuccess() {
		
		// both players have 40 points 
		for (int ii = 0; ii < 3; ii++) {
			this.tennisTest.update(this.playerA);
			this.tennisTest.update(this.playerB);
		}
				
		// playerB wins one more time
		System.setOut(new PrintStream(outContent));
		this.tennisTest.update(this.playerB);		
			
		assertEquals("40-40A", outContent.toString());		
	}

	@Test
	public void testUpdatePlayerWinWithoutAdvantageSuccess() {
		
		// playerA has 40 points
		for (int ii = 0; ii < 3; ii++) {
			this.tennisTest.update(this.playerA);	
		}				

		// playerA wins one more time
		System.setOut(new PrintStream(outContent));
		this.tennisTest.update(this.playerA);
		
		assertTrue(this.tennisTest.isGameOver());
	}

	@Test
	public void testUpdatePlayerAdvantageSuccess() {
		
		// both players have 40 points 
		for (int ii = 0; ii < 3; ii++) {
			this.tennisTest.update(this.playerA);
			this.tennisTest.update(this.playerB);
		}
				
		// playerA wins one more time
		System.setOut(new PrintStream(outContent));
		this.tennisTest.update(this.playerA);		
		
		assertTrue(this.playerA.hasAdvantage());
	}
	
	@Test
	public void testUpdatePlayerEqualitySuccess() {
		
		// both players have 40 points 		
		for (int ii = 0; ii < 3; ii++) {
			this.tennisTest.update(this.playerA);
			this.tennisTest.update(this.playerB);
		}		
		
		// playerA wins one more time and gets advantage
		this.tennisTest.update(this.playerA);
		assertTrue(this.playerA.hasAdvantage());
		
		// playerB wins, playerA loose advantage	
		this.tennisTest.update(this.playerB);		
		assertFalse(this.playerA.hasAdvantage());
	}
	
	@Test
	public void testUpdatePlayerWinWithAdvantageSuccess() {
		
		// both players have 40 points 		
		for (int ii = 0; ii < 3; ii++) {
			this.tennisTest.update(this.playerA);
			this.tennisTest.update(this.playerB);
		}		
		
		// playerA wins one more time and gets advantage
		this.tennisTest.update(this.playerA);
		
		// playerA wins one more time and wins the game
		this.tennisTest.update(this.playerA);
		
		assertTrue(this.tennisTest.isGameOver());
	}
}
