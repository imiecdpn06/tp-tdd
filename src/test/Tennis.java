package test;

import tennis.Player;

/**
 * Tennis
 */
public class Tennis {
	
	private Player playerA;
	
	private Player playerB;
	
	private boolean gameOver = false;
	
	/**
	 * constructor
	 * 
	 * @param Player playerA
	 * @param Player playerB
	 */
	public Tennis(Player playerA, Player playerB) {
		this.playerA = playerA;
		this.playerB = playerB;
	}
	
	/**
	 * isGameOver
	 * 
	 * @return boolean
	 */
	public boolean isGameOver() {
		return this.gameOver;
	}
	
	/**
	 * update
	 * 
	 * @param Player player who has won
	 */
	public void update(Player player) {
		
		Player opponent;
		
		// deduct opponent Player instance from given player
		if(player.equals(this.playerA)) {
			opponent = this.playerB;
		} else {
			player = this.playerB;
			opponent = this.playerA;
		}
		
		// both players have 40 points
		if(player.getScore() == 40 && opponent.getScore() == 40) {
			
			// if player already has an advantage, he wons the game
			if(player.hasAdvantage()) {
				this.gameOver = true;
				return;
			}
				
			// if his opponent has an advantage, he lost it	
			else if(opponent.hasAdvantage()) {
				opponent.setAdvantage(false);
			} 
			
			// if player has no advantage yet, grant it
			else {
				player.setAdvantage(true);	
			}					
		} 
		
		// player has 40 points and opponnent has no advantage
		else if(player.getScore() == 40 && opponent.getScore() < 40) {
			this.gameOver = true;
			return;
		}			
		
		// manage other cases
		else {
			player.incrementScore();	
		}
		
		this.displayScore();
	}
	
	/**
	 * displayScore
	 */
	public void displayScore() {
		
		String stringifiedPlayerAscore = Integer.toString(this.playerA.getScore());
		String stringifiedPlayerBscore = Integer.toString(this.playerB.getScore());
		stringifiedPlayerAscore = (!this.playerA.hasAdvantage()) ? stringifiedPlayerAscore : stringifiedPlayerAscore + "A";
		stringifiedPlayerBscore = (!this.playerB.hasAdvantage()) ? stringifiedPlayerBscore : stringifiedPlayerBscore + "A";
		
		String displayScore = stringifiedPlayerAscore + '-' + stringifiedPlayerBscore;
		
		System.out.print(displayScore);
	}
}
