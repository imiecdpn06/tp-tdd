package tennis;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

	private Player playerTest;
	
	@Before
	public void initialize() {
		
		playerTest = new Player("A");
	}
	
	@Test
	public void testIncrementScoreFromZeroSuccess() {
		
		this.playerTest.incrementScore();
		
		assertEquals(15, this.playerTest.getScore());
	}
	
	@Test
	public void testIncrementScoreFromZeroFailure() {
		
		this.playerTest.incrementScore();
		
		assertNotEquals(0, this.playerTest.getScore());
		assertNotEquals(30, this.playerTest.getScore());
		assertNotEquals(40, this.playerTest.getScore());
	}
	
	@Test
	public void testIncrementScoreFromFifteenSuccess() {
		
		for (int ii = 0; ii < 2; ii++) {
			this.playerTest.incrementScore();	
		}		
		
		assertEquals(30, this.playerTest.getScore());
	}
	
	@Test
	public void testIncrementScoreFromFifteenFailure() {
		
		for (int ii = 0; ii < 2; ii++) {
			this.playerTest.incrementScore();	
		}		
		
		assertNotEquals(0, this.playerTest.getScore());
		assertNotEquals(15, this.playerTest.getScore());
		assertNotEquals(40, this.playerTest.getScore());
	}
	
	@Test
	public void testIncrementScoreFromThirtySuccess() {
		
		for (int ii = 0; ii < 3; ii++) {
			this.playerTest.incrementScore();	
		}		
		
		assertEquals(40, this.playerTest.getScore());
	}
	
	@Test
	public void testIncrementScoreFromThirtyFailure() {

		for (int ii = 0; ii < 3; ii++) {
			this.playerTest.incrementScore();	
		}
		
		assertNotEquals(0, this.playerTest.getScore());
		assertNotEquals(15, this.playerTest.getScore());
		assertNotEquals(30, this.playerTest.getScore());
	}
}