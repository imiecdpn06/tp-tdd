package tennis;

/**
 * Player
 */
public class Player {

	private String name;
	
	private int score = 0;
	
	private boolean advantage = false;
	
	/**
	 * constructor
	 * 
	 * @param String name
	 */
	public Player(String name) {
		this.name = name;
	}
	
	/**
	 * incrementScore
	 * 
	 * @return Player
	 */
	public Player incrementScore() {
		
		if(this.score == 30) {
			this.score += 10;
		} else {
			this.score += 15;
		}
		
		return this;
	}

	/**
	 * getScore
	 * 
	 * @return int
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * hasAdvantage
	 * 
	 * @return boolean
	 */
	public boolean hasAdvantage() {
		return this.advantage;
	}

	/**
	 * setAdvantage
	 * 
	 * @param boolean advantage
	 */
	public void setAdvantage(boolean advantage) {
		this.advantage = advantage;
	}
}